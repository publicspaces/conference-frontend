export default (req, res, next) => {
  res.setHeader('Cache-Control', 'max-age=300, stale-while-revalidate');
  next()

}