// strapi localize helper


export default (context, inject) => {
  const localize = (item, attribute) => {
  	// return if none or invalid item
  	if (!item || !item.attributes) return;
  	// if item is in current locale, return it
  	if (item.attributes.locale == context.i18n.locale) return item.attributes[attribute]
  	// if no localizations are available, return default
  	if (!item.attributes.localizations) return item.attributes[attribute];
  	// check if there is a localized version for the current locale
  	const localized = item.attributes.localizations.data.find(l=>l.attributes.locale == context.i18n.locale);
  	// return if found
    if (localized) return localized.attributes[attribute];
    // fallback on default locale if no localized data is found
    return item.attributes[attribute]
  }
  // Inject $localize(item, attribute) in Vue, context and store.
  inject('localize', localize)

  const flag = (lang) => {
  	if (lang == 'nl') return '🇳🇱';
  	if (lang == 'en') return '🇬🇧';
  	return '🇪🇺';
  }
  inject('flag', flag)

  const localTimes = (start, end) => {
  	return context.$moment(start).format("HH:mm") + (end?" - "+end.substr(0,5):"")
  }
  inject ('localTimes', localTimes);

  const localDate = (date, type) => {

    let format = context.i18n.locale=='nl'?"DD MMMM":"MMMM DD";
    if (type == 'FULL') format = context.i18n.locale=='nl'?"dddd DD MMMM":"dddd MMMM DD";
  	return context.$moment(date).locale(context.i18n.locale).format(format);
  }
  inject ('localDate', localDate);

}