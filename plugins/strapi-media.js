// strapi avatars helper


export default (context, inject) => {
  const ava = (avatar) => {
  	return img(avatar);
  }
  inject ('ava', ava);
  const img = (img, format = 'medium') => {
    if (!img) return "";
    if (!img.data) return "";
    
    if (img.data.attributes.mime == 'image/svg+xml') {
      return `${context.$config.strapiUrl}/..${img.data.attributes.url}`
    }

    if (img.data.attributes.formats === null) {
      return "";
    }

    if (!img.data.attributes.formats[format]) {
      if (img.data.attributes.formats.large) format = 'large';
      else if (img.data.attributes.formats.medium) format = 'medium';
      else if (img.data.attributes.formats.small) format = 'small';
      else if (img.data.attributes.formats.thumbnail) format = 'thumbnail';
      else return "";
    }

  	const url = _.get(img,`data.attributes.formats.${format}.url`);
  	if (!url) return "";
  	return `${context.$config.strapiUrl}/..${url}`
  }
  inject ('img', img);

  const apiImages = (content) => {
    return content.replaceAll(`src="/uploads`, `src="${context.$config.strapiUrl}/../uploads`)
  }
  inject('apiImages',apiImages)
}