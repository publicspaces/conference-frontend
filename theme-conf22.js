export default {
    fonts: {
        heading: 'geomanist, sans-serif',
        body: 'geomanist, sans-serif',
    },
    fontWeights: {
        bold: 500,
    },
    colors: {
        'ps-red': '#e1231e',
        'ps-grey-dark': '#3c3c3c',
        'ps-grey-light': '#ececec'
    },
}