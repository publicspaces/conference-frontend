export const state = () => ({
   tracks: [],
   events: [],
   bazaar: [],
   pages: [],
   locations: [],
   people: [],
   editions: [],
   homepage: {},
   news: [],
   settings: {},
   now: new Date()
})


export const mutations = {
  setTracks(state, payload) {
    state.tracks = payload;
  },
  setEvents(state, payload) {
    state.events = payload;
  },
  setPages(state, payload) {
    state.pages = payload;
  },
  setLocations(state, payload) {
    state.locations = payload;
  },
  setPeople(state, payload) {
    state.people = payload;
  },
  setEditions(state, payload) {
    state.editions = payload;
    state.homepage = payload[0];
  },
  setNews(state, payload) {
    state.news = payload;
  },
  setBazaar(state, payload) {
    state.bazaar = payload;
  },
  setSettings(state, payload) {
    state.settings = payload;
  },
  updateNow(state, payload) {
    state.now = new Date();
  }
}

export const getters = {
  trackBySlug: (state) => (slug) => {
    return state.tracks.find(track => track.attributes.slug === slug);
  },
  eventBySlug: (state) => (slug) => {
    return state.events.find(event => event.attributes.slug === slug);
  },
  eventsByLocation: (state) => (location) => {
    return state.events.filter(event => {
      if (!event.attributes.location.data) return false
      return event.attributes.location.data.id === location

    });
    // const now = new Date("2022-05-17 15:00:00");
    // let upcoming = state.events.filter(event => {
    //   const e = new Date(event.attributes.start);
    //   return e>now;
    // });
    // return upcoming[0];
  },
  editionByYear: (state) => (year) => {
    return state.editions.find(edition => edition.attributes.year == year);
  },
  pageBySlug: (state) => (slug) => {
    return state.pages.find(page => page.attributes.slug === slug);
  },
  personBySlug: (state) => (slug) => {
    return state.people.find(person => person.attributes.slug === slug);
  },
  locationById: (state) => (id) => {
    return state.locations.find(l => l.id === parseInt(id));
  },
  homepage: (state) => () => {
    return state.editions[0];
  }
}

export const actions = {
  updateNow({ state, commit }, payload) {
     commit('updateNow')
  },
  async nuxtServerInit({ commit }, { $strapi }){


     const settings = (await $strapi.find('setting',{
          locale: 'en',
          populate:'default_avatar,edition,edition.localizations,edition.dates,edition.highlights, edition.poster,edition.sponsors.logo,edition.localizations.poster,edition.tracks,edition.tracks.localizations'
        }));
     commit ('setSettings', settings.data);

     const editions = (await $strapi.find('editions',{
          locale: 'en',
          populate:'localizations,poster,sponsors.logo,dates,highlights,localizations.poster,tracks,tracks.localizations'
        }));
     commit ('setEditions', editions.data);

     const news = (await $strapi.find('news',{
          locale: 'en'
     }));
     commit ('setNews', news.splice(0,3))

     const tracks = (await $strapi.find('tracks',{
        locale: 'en',
        'populate':'localizations',
        sort: 'order',
      })).data;
     commit ('setTracks', tracks)
     const events = (await $strapi.find('events',{
        locale: 'en',
        populate:'localizations,image,hosts,speakers,speakers.avatar,speakers.localizations,hosts.localizations,hosts.avatar,tracks,location,location.stream,location.localizations', 
        sort: 'start',
        'pagination[pageSize]':1000,
        'pagination[page]':1
        }));
     commit ('setEvents', events.data)
     const pages = (await $strapi.find('pages',{
        locale: 'en',
        populate:'localizations,localizations.jumbos,image,jumbos,jumbos.image,jumbos.button,localizations.jumbos.image,localizations.jumbos.button',
        sort: 'navigation_order',
        'pagination[pageSize]':1000,
        'pagination[page]':1
        }));
     commit ('setPages', pages.data)
     const people = (await $strapi.find('people',{
        locale: 'en',
        populate:'localizations,avatar,contact,links,events,events.location',
        'pagination[pageSize]':1000,
        'pagination[page]':1
        }));
     commit ('setPeople', people.data)
     const bazaar = (await $strapi.find('bazaars',{
          locale: 'en',
          populate:'localizations,image,contact',
          sort: 'order:desc',
        }));
     commit ('setBazaar', bazaar.data);
     const locations = (await $strapi.find('locations',{
        locale: 'en',
        populate:'localizations,stream', 
        sort: 'order',
        'pagination[pageSize]':1000,
        'pagination[page]':1
        }));
     commit ('setLocations', locations.data)



  }
}