export default {

  'site.title':'PublicSpaces Conference',
  'site.nav':'Conference',

  'signup.title':'Sign-up now',
  'signup.subtitle':'and receive the latest updates about the PublicSpaces conference in your mailbox',
  'signup.heading': 'Yes, sign me up...',
  'signup.name': 'Name',
  'signup.name.helper': 'We use your name to send you personal emails. If you do not want to reveal your real name, you can also choose a pseudonym.',
  'signup.name.invalid': 'Entering a name is required.',
  'signup.email': 'Email address',
  'signup.email.helper': 'We will use your email address to send you the latest information about the upcoming conference. We never share your email address with third parties.',
  'signup.email.invalid': 'Entering a valid email address is required.',
  'signup.newsletter': 'Subscribe to the PublicSpaces newsletter',
  'signup.newsletter.helper': 'We send a newsletter (in Ducth) about the mission of PublicSpaces once every 6 weeks. If you would like to receive this newsletter, please indicate so here.',
  'signup.submit': 'Register',
  'signup.reset': 'Register another person',

  'signup.success.title': '👍 Success',
  'signup.success.body': `Sign-up for the PublicSpaces conference newsletter has been successful. <br />
                           We will keep you informed via email about the ticket sales, the program, the location and what you can do to
                           come to the conference well prepared.<br /><br />
                           Do you want to get in the mood now? Checkout the <a style="text-decoration: underline" href="https://video.publicspaces.net/c/pubconf/videos">videos of
                           the PublicSpaces Conferences of 2021 ans 2022</a>`,

  'signup.error.title': '😮 Oops! Something went wrong.',
  'signup.error.body': `Unfortunately, something went wrong while processing your registration. Please try again.<br /><br />
                      If things continue to go wrong, please contact us.`,


  'lobby.program':'See the full program per day:',
  'lobby.tracks':'or view the program by theme / track:',
  'lobby.news':'Latest updates',
  'lobby.toots.more':'More updates by @publicspaces ...',
  'lobby.sponsors':'Made possible by',
  'lobby.sponsors.body':'Organising this conference was made possible by contributions from these organisations.',
  'lobby.tickets':'Purchase your Ticket',
  'lobby.signup':'Stay up to date',
  'lobby.route':'Route to Pakhuis de Zwijger',
  'lobby.route_link':'https://dezwijger.nl/about-us-en/contact',
  'lobby.follow.mastodon':'Follow @PublicSpaces on Mastodon',
  'lobby.follow.matrix':'Join the conversation on Matrix',
  'lobby.watchbutton':'Watch and chat',
  'lobby.nownext':'Now & next',
  'lobby.highlights':'Some highlights:',
  'lobby.highlights.more':'Watch all sessions on-demand',

  'track.other':'Other tracks:',
  'event.language':'Language',
  'event.more':'More info',
  'event.hostedby':'Hosted by',
  'event.language.nl':'Language: Dutch.',
  'event.language.en':'Language: English.',
  'event.language.nl+en':'Language: Dutch with English subtitles.',
  'event.pepper':'All sessions are open to everyone. Is there a 🌶️ next to the session? That means some prior knowledge is useful.',
   
   'nav.lobby':'Lobby',
   'nav.program':'Program',
   'nav.per.day':'Per day',
   'nav.per.track':'Per track',
   'nav.speakers':'Speakers',
   'nav.signup':'Newsletter',
   'nav.tickets':'Tickets',
   'nav.live':'Live-streams',
   'nav.livestream':'Live-stream',
   'nav.ondemand':'On Demand',
   'nav.archive': 'Archive',
   'nav.bazaar': 'Bazaar',
   'nav.art': 'Art',


   'session.livestream':'Livestream',
   'session.livenotes':'Live Notes',
   'session.notes':'Notes',
   'session.notes_intro':'Minutes were taken during this session. Click on the button below to view the notes.',
   'session.shownotes':'Show notes',
   'session.ondemand':'Watch now',
   'session.hostedby':'Hosted by',
   'session.podcast':'Podcast',
   'session.speakers':'Speakers',
   'session.chat':'Chat',
   'sessions.livestreamed':'This session will be streamed live. The player will appear here the moment the session has started.',
   'sessions.islive':'This session can be attended online and will start around {time} on the {location} Livestream.',
   'session.soon':'More information on this evens will appear here soon.',


   'people.sessions': 'is speaking here:',

   'rsvp.button':'RSVP',
   'rsvp.title':'Let us know you\'re comming',
   'rsvp.body':'In order to estimate the interest in this session, we would like to take stock of how many people want to come. Please leave your e-mail address or matix ID below so we know you are coming.',
   'rsvp.input':'Email address of Matrix ID',
   'rsvp.contact.invalid':'Please enter a valid e-mail address or Matrix ID',
   'rsvp.contact.helper':'We only use this data to gauge interest. All data will be deleted after the conference.',
   'rsvp.button.signup':'I\'m coming',
   'rsvp.button.cancel':'Never mind',
   'rsvp.success.title':'It worked!',
   'rsvp.success.body':'Your registration is processed',
   'rsvp.error.title':'Oops!',
   'rsvp.error.body':'Something has gone wrong. Please try again later.He',

  'bio.title': 'Speaker Profile',
  'bio.subtitle': 'To display a biography and profile on the website, we ask you to provide some information',
  'bio.heading': 'Personal Details',
  'bio.name': 'Name',
  'bio.name.helper': 'How would you like us to mention your name in the programme?',
  'bio.name.invalid': 'Name is a required field.',

  'bio.role': 'Role and Organisation',
  'bio.role.helper': 'Provide a brief description of your role or position and - if applicable - the organisation you work for. For example, "CTO PublicSpaces"',
  'bio.role.invalid': 'Please enter a valid role',

  'bio.bio': 'Biography and/or highlights (preferably in English)',
  'bio.bio.helper': 'A brief description of your work or yourself. You may include any important matters or links you want to have mentioned. The text will be edited and translated by the editorial team.',
  'bio.bio.invalid': 'Please complete ...',

  'bio.avatar': 'Profile Picture',
  'bio.avatar.helper': 'Upload a nice profile picture. Preferably square and in colour.',
  'bio.avatar.invalid': 'The file format must be jpg, png or gif.',

  'bio.contact': 'Which contact details can we display?',

  'bio.email': 'Email Address',
  'bio.email.helper': 'If we may display a public email address on your profile, please enter it here.',
  'bio.email.invalid': 'Please enter an email address. In the form of "name@server"',

  'bio.twitter': 'Twitter Handle',
  'bio.twitter.helper': 'Are you on Twitter and would like your handle to be shown on your profile page? If so, please enter your handle here.',
  'bio.twitter.invalid': 'The handle does not look correct. The handle always starts with an @. For example, "@public_spaces"',

  'bio.matrix': 'Matrix',
  'bio.matrix.helper': 'If participants may contact you via Matrix, please enter your Matrix address here.',
  'bio.matrix.invalid': 'A Matrix account is in the form of "user:server"',

  'bio.activitypub': 'Mastodon / Fediverse',
  'bio.activitypub.helper': 'Enter your Mastodon account here if you would like participants to be able to follow you on the Fediverse.',
  'bio.activitypub.invalid': 'A valid account name is in the form name@instance. For example, "newsroom@publicspaces.net"',

  'bio.consent': 'Agree?',
  'bio.consent.helper': 'We will only process this information for publication on the PublicSpaces conference website.',

  'bio.submit': 'Submit',

  'bio.success.title': 'Success 🎉!',
  'bio.success.body': 'We have received and processed the information. If you still wish to modify the details, you can use the link again.',

  'bio.error.title': 'Oops 🙊!',
  'bio.error.body': 'Something went wrong during processing. Please try again later or contact team@publicspaces.net.',

   'toot.replies':'Replies',
   'toot.reblogs':'Reblogs',
   'toot.more':'More ...',

   'live.now':'Now:',
   'live.next':'Next:',
   'live.hosts':'Host(s):',
   'live.speakers':'Speakers:',

   'speakers.title':'All speakers',
   'speakers.body':'We welcome the following speakers to the conference.',
   'speaker.presence':'Meet {speaker} during the conference here:',
   'speaker.presence.previous':'During the conference {edition} in {year}, {speaker} was part of the following session(s):',

   'bazaar.title': 'PublicSpaces Bazaar',
   'bazaar.body': 'During this festival full of art, culture and good conversations, there will be plenty of room for visitors to meet providers of concrete solutions and tools at the PublicSpaces Bazaar. See which organisations will be present or plan an appointment in advance.',
   'bazaar.location': 'You will find the Bazaar on Tuesday 27 June in the Foyer on the second floor.',
   'bazaar.cta.speeddate':'Book a Speeddate',
   'bazaar.cta.website': 'Visit website ...',

   'art.title': 'Art & Culture',
   'art.body': 'During this year\'s conference, you can emerge yourself in art and culture. Art, in all its forms and expressions, has always played a unique role in exploring the complex dynamics between humans and technology. With performances, music and visual art, we will create a space where we can ask questions about how technology affects our lives and how we can create meaningful connections in the digital world.',
   'art.location': 'Take a breather from the panels, talks and workshops and get inspired!',


   'archive.title':'Watch all events on demand',
   'archive.sponsors':' was made possible by:',

   'footer.credits':'This website has been compiled with great care, however, no rights can be derived from its content. The <a href=\'https://publicspaces.net/algemene-voorwaarden/\'>General Terms and Conditions of PublicSpaces</a> apply to this website.',
   'footer.copyright':'All written content on this website may be shared freely under a <a href=\'https://creativecommons.org/licenses/by/4.0/\' target=\'_blank\'>CC BY 4.0 licence.',

   'language.nl': 'Dutch',
   'language.en': 'English'
}
