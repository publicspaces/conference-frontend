export default {

  'site.title':'PublicSpaces Conferentie',
  'site.nav':'Conferentie',

  'signup.title':'Meld je aan',
  'signup.subtitle':'en ontvang het laatse nieuws over de PublicSpaces conferentie in je mailbox.',
  'signup.heading': 'Ja, schrijf me in.',
  'signup.name': 'Naam',
  'signup.name.helper': 'We gebruiken je naam om je persoonlijke mails te sturen. Als je je echte naam niet wilt prijsgeven kan je ook een pseudoniem kiezen.',
  'signup.name.invalid': 'Het invoeren van een naam is vereist.',
  'signup.email': 'E-mailadres',
  'signup.email.helper': 'We gebruiken jouw e-mailadres om je de laatste informatie over de aankomende conferentie te sturen. We delen jouw e-mailadres nooit met derden.',
  'signup.email.invalid': 'Het invoeren van een geldig e-mailadres is vereist.',
  'signup.newsletter': 'PublicSpaces nieuwsbrief erbij?',
  'signup.newsletter.helper': 'We sturen eens per 6 weken een nieuwsbrief over de missie van PublicSpaces. Indien je deze nieuwsbrief wilt ontvangen, geef dat dan hier aan.',
  'signup.submit': 'Meld je aan',
  'signup.reset': 'Meld nog iemand aan',

  'signup.success.title': '👍 Gelukt',
  'signup.success.body': `De aanmelding voor de PublicSpaces conferentienieuwsbrief gelukt. <br />
                          We houden je via e-mail op de hoogte over de kaartverkoop, het programma, de locatie en wat je kunt doen om
                          goed voorbereid naar de conferentie te komen.<br /><br />
                          Wil je nu alvast in de stemming komen? Kijk nog eens naar
                           <a style="text-decoration: underline" href="https://video.publicspaces.net/c/pubconf/videos">de video\'s van de PublicSpaces conferenties van 2021 en 2022</a>
                          `,
   'signup.error.title': '😮 Oeps! Niet gelukt.',
   'signup.error.body': `Helaas is er ietsmis gegaan tijdens het verwerken van je inschrijving. Probeer het nog een keer.<br /><br />
                        Mocht het mis blijven gaan, neem dan even contact met ons op.`,

   'lobby.program':'Bekijk het volledige programma per dag:',
   'lobby.tracks':'of bekijk het programma per thema / track:',
   'lobby.news':'Laatste updates',
   'lobby.toots.more':'Meer updates van @publicspaces ...',
   'lobby.sponsors':'Mogelijk gemaakt door',
   'lobby.sponsors.body':'Het organiseren van deze conferentie is mogelijk gemaakt door bijdragen van deze organisaties',
   'lobby.tickets':'Bestel kaarten',
   'lobby.signup':'Blijf op de hoogte',
   'lobby.route':'Route naar Pakhuis de Zwijger',
   'lobby.route_link':'https://dezwijger.nl/over-ons/routebeschrijving',
   'lobby.follow.mastodon':'Volg @PublicSpaces op Mastodon',
   'lobby.follow.matrix':'Praat mee op Matrix',
   'lobby.watchbutton':'Kijk en praat mee',
   'lobby.nownext':'Nu & straks',
   'lobby.highlights':'Enkele hoogtepunten:',
   'lobby.highlights.more':'Bekijk alle sessies on-demand',

   'track.other':'Andere tracks:',
   'event.language':'Taal',
   'event.language.nl':'Taal: Nederlands.',
   'event.language.en':'Taal: Engels.',
   'event.language.nl+en':'Taal: Nederlands met Engelstalige ondertiteling',
   'event.more':'Meer info',
   'event.hostedby':'Presentatie',
   'event.pepper':'Alle sessies zijn toegankelijk voor iedereen. Staat er een 🌶️ bij de sessie? Dat betekent dat enige voorkennis handig is.',
   

   'nav.lobby':'Lobby',
   'nav.program':'Programma',
   'nav.per.day':'Per dag',
   'nav.per.track':'Per track',
   'nav.speakers':'Sprekers',
   'nav.tickets':'Tickets',
   'nav.signup':'Nieuwsbrief',
   'nav.live':'Livestreams',
   'nav.livestream':'Livestream',
   'nav.ondemand':'Kijk terug',
   'nav.archive': 'Archief',
   'nav.bazaar': 'Bazaar',
   'nav.art': 'Kunst',

   'session.livestream':'Livestream',
   'session.livenotes':'Live Notities',
   'session.notes':'Notities',
   'session.notes_intro':'Tijdens deze sessie zijn notulen gemaakt. Klik op de onderstaande knop om de notities weer te geven.',
   'session.shownotes':'Notities weergeven',
   'session.ondemand':'Kijk terug',
   'session.podcast':'Podcast',
   'session.hostedby':'Presentatie',
   'session.speakers':'Sprekers',
   'session.chat':'Chat',
   'sessions.livestreamed':'Deze sessie wordt live gestreamd. De speler zal hier verschijnen op het moment dat de sessie is begonnen.',
   'sessions.islive':'Deze sessie kan online bijgewoond worden en zal rond {time} beginnen op de livestream van de {location}.',
   'session.soon':'Binnenkort verschijnt hier meer informatie over dit programmaonderdeel.',

   'people.sessions': 'spreekt hier:',

   'rsvp.button':'RSVP',
   'rsvp.title':'Laat weten dat je komt',
   'rsvp.body':'Om een inschatting te maken van de belangstelling voor deze sessie willen we graag inventariseren hoeveel mensen willen komen. Laat hieronder je e-mailadres of matix-ID achter zodat we weten dan je komt.',
   'rsvp.input':'E-mailadres of Matrix ID',
   'rsvp.contact.invalid':'Vul een geldig emailadres of Matrix ID in',
   'rsvp.contact.helper':'We gebruiken deze gegevens alleen om de belangstelling te peilen. Na afloop van de conferentie worden alle gegevens gewist.',
   'rsvp.button.signup':'Ik kom',
   'rsvp.button.cancel':'Laat maar',
   'rsvp.success.title':'Gelukt',
   'rsvp.success.body':'Je aanmelding is verwerkt.',
   'rsvp.error.title':'Helaas',
   'rsvp.error.body':'Er is iets fout gegaan. Probeer het later nog eens.',

   'bio.title': 'Sprekers-profiel',
   'bio.subtitle': 'Om een biografie en profiel op de website te tonen vragen we je om enkele gegevens aan te leveren',
   'bio.heading': 'Personalia',
   'bio.name':'Naam',
   'bio.name.helper':'Hoe wil je dat we je naam in het programma vermelden?',
   'bio.name.invalid':'Naam is een verplicht veld.',

   'bio.role': 'Functie en organisatie (bij voorkeur in het Engels)',
   'bio.role.helper':'Geef een korte omschrijving van jouw functie of rol en - indien van toepassing - de organisatie waarvoor je dat doet. Bijvoorbeeld "CTO PublicSpaces" ',
   'bio.role.invalid':'Vul een goede functie in',

   'bio.bio':'Biografie en/of highlights (bij voorkeur in het Engels)',
   'bio.bio.helper':'Een korte beschrijving van je werk of jezelf. Vermeld eventueel belangrijke zaken of links die je vermeld wilt hebben. De tekst zal worden bewerkt en vertaald door de redactie.',
   'bio.bio.invalid': 'Vul aan ...',

   'bio.avatar':'Profielfoto',
   'bio.avatar.helper':'Upload een leuke profielfoto. Het liefst vierkant en in kleur.',
   'bio.avatar.invalid':'Het bestandsformaat moet jpg, png of gif zijn.',

   'bio.contact': 'Welke contactgegevens mogen we vermelden?',

   'bio.email': 'E-mailadres',
   'bio.email.helper': 'Indien we een publiek e-mailadres bij je profiel mogen vermelden, vul dat dan hier in. ',
   'bio.email.invalid':'Vul een e-mailadres in. In de vorm van "naam@server"',

   'bio.twitter': 'Twitter handle',
   'bio.twitter.helper': 'Ben je te volgen via Twitter en wil je je handle tonen op jouw profielpagina? vul dan hier je handle in.',
   'bio.twitter.invalid':'De handle ziet er niet goed uit. De handle begint altijs met een @. Bijvoorbeel "@public_spaces"',

   'bio.matrix': 'Matrix',
   'bio.matrix.helper': 'Als deelnemers jou via Matrix mogen contacten, vul hier je matrixadres in.',
   'bio.matrix.invalid':'Een matrix-account heeft de vorm van "user:server"',

   'bio.activitypub': 'Mastodon / Fediverse',
   'bio.activitypub.helper': 'Vul je Mastodon account hier in als je wilt dat deelnemers jou kunnen volgen op het Fediverse.',
   'bio.activitypub.invalid':'Een geldige accountnaam heeft de vorm naam@instance. Bijvoorbeeld "newsroom@publicspaces.net"',

   'bio.consent': 'Akkoord?',
   'bio.consent.helper': 'Wij verwerken deze gegevens alleen voor publicatie op de website van de PublicSpaces conferentie.',

   'bio.submit':'Stuur in',

   'bio.success.title':'Gelukt 🎉!',
   'bio.success.body':'We hebben de gegevens ontvangen en verwerkt. Mocht je de gegevens toch nog willen aanpassen dan kan je de link opnieuw gebruiken.',

   'bio.error.title':'Oeps 🙊!',
   'bio.error.body':'Er is iets niet goed gegaan bij de verwerking. Probeer het later nog een keer of neem contact op het team@publicspaces.net.',


   'toot.replies':'Reacties',
   'toot.reblogs':'Reblogs',
   'toot.more':'Meer ...',

   'live.now':'Nu:',
   'live.next':'Straks:',
   'live.hosts':'Presentatie:',
   'live.speakers':'Sprekers:',

   'speakers.title':'Alle sprekers',
   'speakers.body':'De volgende sprekers verwelkomen wij op de conferentie.',
   'speaker.presence':'Ontmoet {speaker} op de conferentie hier:',
   'speaker.presence.previous':'Tijdens de conferentie {edition} in {year} was {speaker} onderdeel van de volgende sessie(s):',

   'bazaar.title': 'PublicSpaces Bazaar',
   'bazaar.body': 'Tijdens dit festival vol kunst, cultuur en goede gesprekken, is voor bezoekers volop ruimte om kennis te maken met aanbieders van concrete oplossingen en tools op de PublicSpaces Bazaar. Bekijk alvast welke organisaties aanwezig zullen zijn of plan alvast een afspraak.',
   'bazaar.location': 'Je vindt de Bazaar op dinsdag 27 juni in de Foyer op de tweede etage.',
   'bazaar.cta.speeddate':'Plan een Speeddate',
   'bazaar.cta.website': 'Bezoek website ...',

   'art.title': 'Kunst & Cultuur',
   'art.body': 'Tijdens de conferentie kan je dit jaar flink cultuur snuiven. Kunst, in al zijn vormen en expressies, heeft altijd een unieke rol gespeeld in het onderzoeken van de complexe dynamiek tussen mens en technologie. Met performances, muziek en beeldende kunst creëren we een ruimte waarin we diepgaande vragen kunnen stellen over hoe technologie ons leven beïnvloedt en hoe we betekenisvolle verbindingen kunnen creëren in de digitale wereld. ',
   'art.location': 'Neem even een adempauze van de panels, talks en workshops en laat je inspireren! ',

   'archive.title':'Kijk de opnamen nog eens rustig terug',
   'archive.sponsors':' is mogelijk gemaakt door:',

   'footer.credits':'Deze website is met grote zorg samengesteld, echter aan de inhoud kunnen geen rechten worden ontleend. Op deze website zijn de <a href=\'https://publicspaces.net/algemene-voorwaarden/\'>Algemene Voorwaarden van PublicSpaces</a> van toepassing.',
   'footer.copyright':'Alle geschreven inhoud op deze website mag vrij worden gedeeld onder een <a href=\'https://creativecommons.org/licenses/by/4.0/\' target=\'_blank\'>CC BY 4.0 licentie.',

   'language.nl': 'Nederlands',
   'language.en': 'Engels'
}
