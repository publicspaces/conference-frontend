
const bodyParser = require('body-parser')
const app = require('express')()
const axios = require('axios');

const LISTMONK_URL = process.env.LISTMONK_URL;
const LISTMONK_USERNAME = process.env.LISTMONK_USERNAME;
const LISTMONK_PASSWORD = process.env.LISTMONK_PASSWORD;
const LISTMONK_CONFERENCE_LIST = parseInt(process.env.LISTMONK_CONFERENCE_LIST);
const LISTMONK_NEWSLETTER_LIST = parseInt(process.env.LISTMONK_NEWSLETTER_LIST);


app.use(bodyParser.json())
app.all('/signup', async (req, res) => {
  try{

  	if (!req.body.email.match(/^[a-zA-Z0-9_.+]+(?<!^[0-9]*)@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/)) {
  		throw('invalid email address')
  	}

  	// test if user is already subscribed to any list:
  	const check = await axios.get(`${LISTMONK_URL}/api/subscribers?query=email%3D'${encodeURIComponent(req.body.email)}'`,{
	  	auth: {
		    username: LISTMONK_USERNAME,
		    password: LISTMONK_PASSWORD
		}
	  });
  	
	  if(check.data.data.total > 0) {
	  	// existing subscriber
	  	const response  = await axios.put(`${LISTMONK_URL}/api/subscribers/lists`,{
	  			ids: [ check.data.data.results[0].id ],
			    action: 'add',
			    status: 'unconfirmed',
			    target_list_ids: req.body.newsletter == true ? [LISTMONK_CONFERENCE_LIST,LISTMONK_NEWSLETTER_LIST] :[LISTMONK_CONFERENCE_LIST]
			},{
		  	auth: {
			    username: LISTMONK_USERNAME,
			    password: LISTMONK_PASSWORD
			}
		  })
		  // send confirmation
		  const optin  = await axios.post(`${LISTMONK_URL}/api/subscribers/${check.data.data.results[0].id}/optin`,{
	 
			},{
		  	auth: {
			    username: LISTMONK_USERNAME,
			    password: LISTMONK_PASSWORD
			}
		  })

	  } else {
	  	// new subscriber
	  	const response  = await axios.post(`${LISTMONK_URL}/api/subscribers`,{
	  			email: req.body.email,
			    name: req.body.name,
			    status: 'enabled',
			    lists: req.body.newsletter == true ? [LISTMONK_CONFERENCE_LIST,LISTMONK_NEWSLETTER_LIST] :[LISTMONK_CONFERENCE_LIST]
			},{
		  	auth: {
			    username: LISTMONK_USERNAME,
			    password: LISTMONK_PASSWORD
			}
		  })

	  }

	  //return;



  	
	  console.log(`Successful signup for ${req.body.name}`)
	  res.json({ status: 'succes' })

  } catch(e) {
  	console.error(`Error while signup for ${req.body.name}`)
  	console.error(e)
  	res.status(400);
  	res.json({ status: 'error' })
  }
})

module.exports = app