export default {
  computed: {
	  dates() {
	  	return this.$store.state.settings.attributes.edition.data.attributes.dates.map((d,i)=>{
	  		return {id:i,text:this.$localDate(d.start),full:this.$localDate(d.start, 'FULL'), slug:this.$moment(d.start).format("YYYY-MM-DD")}
	  	});
	  },
	  showtime() {
	  	if (this.$route.query.showtime) return true;
	  	if (this.$store.state.settings.attributes.showtime == true) return true;
	  	//if (!this.$store.state.homepage.attributes.dates) return false;
	  	const now = this.$moment();
	  	for (let d of this.dates) {
	  		if (this.$moment(d.start).isBefore(now) && this.$moment(d.end).isAfter(now)) return true;
	  	}
	  	return false;

	  },
	  tracks () {
	    return  this.$store.state.settings.attributes.edition.data.attributes.tracks.data
	  },
	  currentEdition() {
	  	return this.$store.state.settings.attributes.edition.data
	  	//return this.$store.state.editions.find(e=> this.$store.state.settings.attributes.edition.data.id == e.id);
	  },
	  streamedLocations() {
	  	return this.$store.state.locations.filter(l=>{
	  		if (!l.attributes.stream) return false;
	  		if (l.attributes.stream.data) return true;
	  		return false;
	  	})
	  },
	  settings() {
	  	return this.$store.state.settings.attributes;
	  },
	  now() {
	  	return this.$store.state.now;
	  }
  },

}