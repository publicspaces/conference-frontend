import Theme from './theme-conf22.js'
import webpack from 'webpack'

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'pubconf',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://cdn.publicspaces.net/web/theme/latest/style.css' }
    ],
    script: [
      {
          type: 'text/javascript',
          src: '/js/cactus.js',
          body: true
      }
    ]
  },

  ssr: true,

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
   './assets/css/main.css',
  ],

  publicRuntimeConfig: {
    strapiUrl: process.env.STRAPI_URL,
    plausible: {
      domain: process.env.PLAUSIBLE_DOMAIN,
      apiHost: process.env.PLAUSIBLE_API_HOST
    }
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/localize.js',
    '~/plugins/strapi-media.js',
    '~/plugins/jsonld'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/moment',
    'vue-plausible'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/chakra
    //'@chakra-ui/nuxt',
    // https://go.nuxtjs.dev/emotion
    '@nuxtjs/emotion',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://i18n.nuxtjs.org/
    '@nuxtjs/i18n',
    // strapi
    '@nuxtjs/strapi',
    // style resources
    '@nuxtjs/style-resources'
  ],

  moment: {
    defaultLocale: 'en-gb',
    locales: ['nl','en-gb'],
    timezone: true,
    defaultTimezone: 'Eurpoe/Amsterdam'
  },

  plausible: { // Use as fallback if no runtime config is available at runtime
    domain: process.env.PLAUSIBLE_DOMAIN,
    apiHost: process.env.PLAUSIBLE_API_HOST,
    enableAutoPageviews: true
  },

  chakra: {
    extendTheme: Theme
  },

  styleResources: {
    scss: ['./assets/scss/*.scss']
  },

  serverMiddleware: [
    { path: "/api", handler: "~/api/signup.js" },
    '~/middleware/cache.js'
  ],

  strapi: {
    url: process.env.STRAPI_URL || 'http://localhost:1337/api',
    entities: [
      { name: 'homepage', type: 'single' },
      { name: 'attendees' }
    ]
  },

  i18n: {
    locales: [{ code: 'en', iso: 'en-US', file: 'en.js', name: 'English', icon: '🇬🇧' }, { code: 'nl', iso: 'nl-NL', file: 'nl.js', name: 'Nederlands', icon: '🇳🇱' }],
    defaultLocale: 'nl',
    langDir: '~/locales/',
    detectBrowserLanguage: false,
    oldValue: { alwaysRedirect: false, fallbackLocale: '', redirectOn: 'root', useCookie: false, cookieCrossOrigin: false, cookieDomain: null, cookieKey: 'i18n_redirected', cookieSecure: false },
    vueI18n: {
      fallbackLocale: 'en',
      messages: {

      }
    }
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/',
  },

  router: {
    prefetchLinks: false
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    postcss: {
      //postcssOptions: {
        plugins: {
          'tailwindcss/nesting': {},
          tailwindcss: {},
          autoprefixer: {},
        },
      //},
    },

    plugins: [
      new webpack.ProvidePlugin({
        // global modules
        _: 'lodash'
      })
    ]
  }
}
